import {types} from '../reduxTypes.js';

// Initial state of the store
const initialState = {
  currentDateCalendar: new Date(),
  time_line: {
      data: {},
      loading: true
  },
  halls: {
      upi: [],
      urgu: [],
      loading: true
  },
  empty_times: {},
  allRooms: []
}

export const scheduleReducers = (state = initialState, action) => {
    const {type, payload} = action

    switch (type) {
        case types.SELECT_DATE: {
            return {
                ...state,
                currentDateCalendar: payload
            }
        }

        case types.FETCH_ROOMS: {
            return {
                ...state,
                halls: {
                    upi: [],
                    urgu: [],
                    loading: true,
                    errors: null
                }
            }
        }

        case types.FETCH_ROOMS_SUCCESS: {
            return {
                ...state,
                halls: {
                    ...payload,
                    loading: false,
                    error: null
                }
            }
        }

        case types.FETCH_ROOMS_ERROR: {
            return {
                ...state,
                halls: {
                    upi: [],
                    urgu: [],
                    loading: false,
                    error: payload
                }
            }
        }

        case types.SELECT_TIME_LINE: {
            let timeLineData = Object.assign({}, state.time_line.data);
            timeLineData[payload].selected = !timeLineData[payload].selected;
            return {
                ...state,
                time_line: {
                    data: timeLineData,
                    loading: false,
                    error: null
                }
            }
        }

        case types.FETCH_TIME_LINES: {
            return {
                ...state,
                time_line: {
                    data: [],
                    loading: true,
                    errors: null
                }
            }
        }

        case types.FETCH_TIME_LINES_SUCCESS: {
            return {
                ...state,
                time_line: {
                    data: payload.data,
                    loading: false,
                    errors: null
                },
                empty_times: payload.emptyTimes,
                allRooms: payload.rooms
            }
        }

        case types.FETCH_TIME_LINES_ERROR: {
            return {
                ...state,
                time_line: {
                    data: [],
                    loading: false,
                    errors: payload
                }
            }
        }
    }

    return state
}
