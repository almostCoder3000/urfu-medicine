import {combineReducers} from 'redux';
import {userReducers} from './UserReducers';
import {scheduleReducers} from './ScheduleReducers';

const allReducers = combineReducers({
    user: userReducers,
    schedule: scheduleReducers
});

export default allReducers;
