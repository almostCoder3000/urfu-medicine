import {AsyncStorage} from 'react-native';
import {types} from '../reduxTypes.js';



// Initial state of the store
const initialState = {
  me: {},
  loading: false
}



export const userReducers = (state = initialState, action) => {
    const {type, payload} = action

    switch (type) {
        case types.LOGOUT: {
            return {
                ...state,
                me: {},
                loading: true
            }
        }

        case types.LOGOUT_SUCCESS: {
            return {
                ...state,
                me: {},
                loading: false
            }
        }

        case types.LOGOUT_ERROR: {
            return {
                ...state,
                error: payload,
                loading: false
            }
        }


        case types.AUTH: {
            return {
                ...state,
                me: {},
                loading: true
            }
        }

        case types.AUTH_SUCCESS: {
            return {
                ...state,
                me: payload,
                loading: false
            }
        }

        case types.AUTH_ERROR: {
            return {
                ...state,
                error: payload,
                loading: false
            }
        }
    }

    return state
}
