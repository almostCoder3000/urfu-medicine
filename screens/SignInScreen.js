import React from 'react';
import { StyleSheet, TextInput, Text, View,
        TouchableOpacity, Alert, AsyncStorage } from 'react-native';
import Svg, {
  Path
} from 'react-native-svg';

import AwesomeButton from '../components/AwesomeButton.js';
import AwesomeTextInput from '../components/AwesomeTextInput.js';
import { connect } from 'react-redux';
import { actionCreators } from '../actions/allActions.js';
//import { Bars } from 'react-native-loader';

const config = require('../configure.js');
import {postRequest} from '../helpers.js';




class SignInScreen extends React.Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props);
        this.state = {
            login: "",
            password: ""
        }
    }

    componentDidMount() {
        var self = this.props;
        AsyncStorage.getItem('login').then((login) => {
            AsyncStorage.getItem('password').then(async function(password) {
                if (login && password) {
                    var isLogin = await self.auth(login, password);

                    if (isLogin) {
                        self.navigation.navigate("Main");
                    };
                }
            })
        })
    }

    async submitForm() {
      let {login, password} = this.state;

      var isLogin = await this.props.auth(login, password);

      if (isLogin) {

          await AsyncStorage.setItem('login', login);
          await AsyncStorage.setItem('password', password);
          this.props.navigation.navigate("Main");
      };

    }

    render() {
        /*let {me, loading} = this.props.user;

        if (loading) {
          return (
              <View style={styles.loadingContainer}>
                  <Bars size={16} color="#BFB788" />
              </View>
          )
        }*/
        return (
            <View style={styles.container}>
                <View style={styles.form}>
                    <View style={styles.logo}>
                        <Svg width="158" height="32" viewBox="0 0 158 32">
                            <Path d="M19.7078 12.928V0.399999H15.8198V13.144C15.8198 14.836 15.2078 16.24 14.2718 17.176C13.2638 18.184 11.8598 18.868 10.2758 18.868C8.65581 18.868 7.28781 18.256 6.27981 17.176C5.30781 16.24 4.65981 14.8 4.65981 13.144V0.399999H0.771813V12.928C0.771813 15.592 1.77981 17.968 3.50781 19.696C5.12781 21.316 7.39581 22.36 10.2758 22.36C12.9398 22.36 15.1718 21.424 16.8278 19.84C18.6638 18.076 19.7078 15.664 19.7078 12.928Z" fill="#32CF86"/>
                            <Path d="M23.3078 0.399999V22H25.1078V2.524H25.3958L34.7918 15.664H35.1878L44.4758 2.524H44.7638V22H46.7438V0.399999H43.9718L35.1158 12.856L26.1878 0.399999H23.3078ZM50.343 22H64.419V20.344H52.287V13.216H62.727V11.56H52.287V5.692H63.987V4H50.343V22ZM67.675 4V22H74.875C77.755 22 80.023 21.064 81.643 19.444C83.227 17.86 84.163 15.592 84.163 13C84.163 10.408 83.227 8.176 81.643 6.592C80.023 4.972 77.755 4 74.875 4H67.675ZM74.731 5.692C77.179 5.692 78.979 6.448 80.239 7.744C81.535 9.076 82.219 10.912 82.219 13C82.219 15.052 81.571 16.852 80.383 18.184C79.123 19.552 77.251 20.344 74.731 20.344H69.619V5.692H74.731ZM87.2219 4V22H89.1299V4H87.2219ZM94.1094 12.964C94.1094 10.696 94.9374 8.86 96.2694 7.528C97.6374 6.124 99.5814 5.368 101.705 5.368C104.297 5.368 105.953 6.16 107.393 7.384L108.509 6.124C106.781 4.54 104.621 3.676 101.705 3.676C98.9334 3.676 96.6294 4.72 94.9014 6.412C93.2094 8.068 92.2014 10.372 92.2014 12.964C92.2014 15.592 93.1734 17.932 94.9014 19.624C96.5934 21.28 98.9334 22.324 101.705 22.324C104.765 22.324 106.781 21.46 108.509 19.876L107.429 18.58C105.989 19.84 104.297 20.632 101.705 20.632C99.5094 20.632 97.5654 19.84 96.1974 18.436C94.8654 17.068 94.1094 15.196 94.1094 12.964ZM111.585 4V22H113.493V4H111.585ZM132.009 4V19.516L119.157 4H117.105V22H118.977V6.448L131.829 22H133.845V4H132.009ZM137.46 22H151.536V20.344H139.404V13.216H149.844V11.56H139.404V5.692H151.104V4H137.46V22Z" fill="#5E5E5E"/>
                        </Svg>
                    </View>
                    <AwesomeTextInput
                        placeholder='Логин'
                        value={this.state.login}
                        onChangeText = { (login) => { this.setState({login}) } }/>
                    <AwesomeTextInput
                        placeholder='Пароль'
                        value={this.state.password}
                        secureTextEntry={true}
                        onChangeText = { (password) => { this.setState({password}) } }/>
                    <AwesomeButton
                        submitForm={this.submitForm.bind(this)}
                        source={require('../assets/icons/arrow_right_white.png')}
                        text='Войти'/>
                    <Text style={styles.authText}>Войти через личный кабинет студента</Text>
                </View>
            </View>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.user
});

const mapDispatchToProps = (dispatch) => {
    return {
        auth: (login, password) => {
            return dispatch(actionCreators.auth(login, password)).payload.then((res) => {
                !res.error ? dispatch(actionCreators.authSuccess(res.user)) :
                dispatch(actionCreators.authError(res.error));
                if (res.error) Alert.alert("Неправильный логин или пароль!");
                console.log('lol');
                return !res.error;
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SignInScreen);


const styles = StyleSheet.create({
  authText: {
    fontFamily: "sf-ui",
    fontSize: 12,
    color: "#2B2B2B",
  },
  logo: {
    marginBottom: 38,
    alignItems: 'center',
    height: 38,
    width: '100%'
  },
  loadingContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff"
  },
  container: {
    justifyContent: 'center',
    width: '100%',
    height: '100%',
    backgroundColor: "white"
  },
  form: {
    width: '80%',
    marginLeft: 'auto',
    marginRight: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    color: "#A2A2A2",
    fontSize: 24,
    marginBottom: 15,
    fontWeight: "100"
  }
});
