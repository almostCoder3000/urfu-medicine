import React from 'react';
import { StyleSheet, TextInput, View } from 'react-native';

export default class AwesomeTextInput extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={{width: '100%'}}>
          <TextInput
              selectionColor='#f6d365'
              placeholder={this.props.placeholder}
              style={styles.text_input}
              value={this.props.text}
              secureTextEntry={this.props.secureTextEntry}
              onChangeText = {this.props.onChangeText}/>
      </View>
    );
  }
}



const styles = StyleSheet.create({
  text_input: {
    padding: 20,
    paddingLeft: 30,
    height: 56,
    fontFamily: "sf-ui",
    fontSize: 14,
    lineHeight: 14,
    backgroundColor: 'white',
    borderRadius: 50,
    marginBottom: 38,
    width: '100%',
    elevation: 1,
    borderWidth: 0
  }
});
