import React from 'react';

import AwesomeButton from './AwesomeButton.js';
import { View, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';

import Svg, {
  Path, Rect
} from 'react-native-svg';

var medicalData = [
  {name: "ЭКГ", status: true, result: true},
  {name: "УЗИ сердца", status: true, result: false},
  {name: "Клинический анализ крови", status: true, result: false},
  {name: "Общий анализ мочи", status: true, result: true},
  {name: "ЛОР", status: false, result: true},
  {name: "Заключение врача-терапевта", status: false, result: true}
]

export default class CertificateCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: false
        }
    }

    open() {
        this.setState({isOpen: !this.state.isOpen});
    }

    _renderMedicalData() {
        return medicalData.map((med, ind) => {
            var colorStatus = {
                backgroundColor: med.status ? med.result ? "#32CF86" : "#FF7373" : "#C4C4C4"
            };
            return (
                <View style={styles.medDataString} key={ind}>
                    <Text style={styles.medName}>{med.name}</Text>
                    <View style={[styles.indicator, colorStatus]}></View>
                </View>
            )
        })
    }

    _renderDownloadActions(code) {
        return (
            <View>
                <View style={styles.topDownloadActions}>
                    <View style={[styles.btnForm, {backgroundColor: "#FF7373"}]}>
                        <Text style={styles.pdfText}>PDF</Text>
                    </View>
                    <Text style={styles.descrText}>Иванов Ивано, {code}, 28.05.2019</Text>
                </View>
                <View style={{flexDirection: "row"}}>
                    <TouchableOpacity style={[styles.btnForm, {elevation: 2}]}>
                        <Svg width="8" height="20" viewBox="0 0 8 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <Path d="M3.64645 19.3536C3.84171 19.5488 4.15829 19.5488 4.35355 19.3536L7.53553 16.1716C7.7308 15.9763 7.7308 15.6597 7.53553 15.4645C7.34027 15.2692 7.02369 15.2692 6.82843 15.4645L4 18.2929L1.17157 15.4645C0.976311 15.2692 0.659728 15.2692 0.464466 15.4645C0.269204 15.6597 0.269204 15.9763 0.464466 16.1716L3.64645 19.3536ZM3.5 0L3.5 19H4.5L4.5 0L3.5 0Z" fill="#5C5C5C"/>
                        </Svg>
                    </TouchableOpacity>
                    <TouchableOpacity style={[styles.btnForm, {elevation: 2}]}>
                        <Svg width="22" height="16" viewBox="0 0 22 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <Rect x="0.5" y="0.5" width="21" height="15" stroke="#5C5C5C"/>
                        <Path d="M1 1L11 11L21 1" stroke="#5C5C5C"/>
                        </Svg>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    render() {
        var percent = {
            width: `${this.props.progress}%`
        }
        var colorNumber = {
            color: this.props.progress === 100 ? "#32CF86" : "#5C5C5C"
        }
        var isOpen = {
            display: this.state.isOpen ? "flex" : "none"
        }
        var rotateArrow = this.state.isOpen ? '180deg' : '0deg';
        return (
            <TouchableOpacity style={styles.container} onPress={this.open.bind(this)}>
                <View><Text style={styles.title}>{this.props.title}</Text></View>
                <View style={styles.bottomPart}>
                    <Text style={[styles.code, colorNumber]}>{this.props.code}</Text>
                    <View style={[styles.progressContainer, {display: this.props.progress === 100 ? "none" : "flex"}]}>
                        <View style={styles.progressBackground}>
                            <View style={[styles.progressBar, percent]}></View>
                        </View>
                        <Image style={{transform: [{rotate: rotateArrow}]}} source={require('../assets/icons/arrow_down.png')} />
                    </View>
                    <View style={[styles.progressContainer, {display: this.props.progress !== 100 ? "none" : "flex"}]}>
                        <View style={{flexDirection: "row"}}>
                            <Text style={styles.dateString}>Действительна до </Text>
                            <Text style={[styles.dateString, {color: "#32CF86"}]}>01.09.2019</Text>
                        </View>
                        <Image style={{transform: [{rotate: rotateArrow}]}} source={require('../assets/icons/arrow_down.png')} />
                    </View>
                </View>
                <View style={{width: "100%", alignItems: this.props.progress === 100 ? "flex-start" : "center"}}>
                    <View style={[styles.details, isOpen]}>
                        <View style={{display: this.props.progress === 100 ? "none" : "flex"}}>
                            <View style={{marginBottom: 25}}>
                                {this._renderMedicalData.bind(this)()}
                            </View>
                        </View>
                        <View style={{display: this.props.progress !== 100 ? "none" : "flex"}}>
                            <View style={{marginBottom: 25}}>
                                {this._renderDownloadActions.bind(this)(this.props.code)}
                            </View>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    elevation: 2,
    margin: 3,
    borderRadius: 20,
    paddingTop: 20,
    paddingBottom: 20,
    paddingLeft: 25,
    paddingRight: 25,
    width: "100%",
    backgroundColor: '#fff',
    marginBottom: 20
  },
  title: {
    fontFamily: 'sf-ui',
    fontSize: 14,
    lineHeight: 17,
    color: "#5C5C5C",
    marginBottom: 19
  },
  code: {
    fontFamily: 'sf-ui',
    fontSize: 24,
    lineHeight: 29,
    color: "#5C5C5C"
  },
  bottomPart: {
    flexDirection: "row"
  },
  progressContainer: {
    flex: 1,
    height: 30,
    marginLeft: 17,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between"
  },
  progressBackground: {
    flex: 1,
    backgroundColor: "#F1F1F1",
    borderRadius: 16,
    height: 10,
    marginRight: 17
  },
  progressBar: {
    flex: 1,
    borderRadius: 16,
    backgroundColor: "#32CF86"
  },
  details: {
    width: "84%",
    paddingTop: 40
  },
  medDataString: {
    height: 30,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 10
  },
  medName: {
    fontSize: 14,
    lineHeight: 17,
    fontFamily: "sf-ui"
  },
  indicator: {
    width: 25,
    height: 25,
    borderRadius: 25
  },
  dateString: {
    fontSize: 12,
    lineHeight: 14,
    color: "#5C5C5C"
  },
  btnForm: {
    width: 62,
    height: 62,
    borderRadius: 20,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    marginRight: 11
  },
  topDownloadActions: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 11
  },
  pdfText: {
    fontFamily: "sf-ui",
    color: "#fff",
    fontSize: 14,
    lineHeight: 17
  },
  descrText: {
    fontFamily: "sf-ui",
    color: "#5C5C5C",
    fontSize: 10,
    lineHeight: 12
  }
});
